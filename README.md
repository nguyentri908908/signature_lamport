# Hướng dẫn sử dụng Web Chữ ký Lamport

### Bước 1: Sao chép dự án từ GitLab

Đầu tiên, bạn cần sao chép dự án Chữ ký Lamport từ GitLab bằng lệnh sau:

```
git clone https://gitlab.com/nguyentri908908/signature_lamport
```

### Bước 2: Cài đặt thư viện Flask

Sau khi sao chép dự án thành công, bạn cần cài đặt thư viện Flask để chạy ứng dụng. Sử dụng pip để cài đặt Flask: 

```
pip install Flask
```

### Bước 3: Chạy ứng dụng Chữ ký Lamport

Sau khi cài đặt Flask, hãy di chuyển vào thư mục dự án "signature_lamport" bằng lệnh:

```
cd Lamport
```

Sau đó, bạn có thể chạy ứng dụng Chữ ký Lamport bằng lệnh sau:

```
python lamport.py
```

