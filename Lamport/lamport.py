from flask import Flask, render_template, request, redirect, url_for
import os
import math
import random
import string
import hashlib
import secrets
import sys
import json
import shutil
import binascii  # Để chuyển đổi bytes thành chuỗi hex
app = Flask(__name__)
main = string.ascii_lowercase
CHAR_ENC = 'utf-8'
BYTE_ORDER = sys.byteorder
SK = 0
PK = 1
# Đường dẫn tới thư mục lưu trữ file JSON
UPLOAD_FOLDER = './uploaded_files'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
# Generate a keypair for a one-time signature
def keygen():
    sk = [[0 for x in range(256)] for y in range(2)]
    pk = [[0 for x in range(256)] for y in range(2)] 
    for i in range(0,256):
        #secret key
        sk[0][i] = secrets.token_bytes(32)
        sk[1][i] = secrets.token_bytes(32)
        #public key
        pk[0][i] = hashlib.sha256(sk[0][i]).digest()
        pk[1][i] = hashlib.sha256(sk[1][i]).digest()

    keypair = [sk,pk]
    return keypair

# Sign messages using Lamport one-time signatures
def signfunc(m, sk):
    sig = [0 for x in range(256)]
    h = int.from_bytes(hashlib.sha256(m.encode(CHAR_ENC)).digest(), BYTE_ORDER)
    for i in range(0,256):
        b = h >> i & 1
        sig[i] = sk[b][i]
    return sig

# Verify Lamport message signatures
def verifyfunc(m, sig, pk):
    h = int.from_bytes(hashlib.sha256(m.encode(CHAR_ENC)).digest(), BYTE_ORDER)
    for i in range(0,256):
        b = h >> i & 1
        check = hashlib.sha256(sig[i]).digest()
        if pk[b][i] != check:
            return False
    return True
# Chuyển đổi danh sách các bytes thành danh sách các chuỗi hex
def bytes_to_hex_string(byte_list):
    return [binascii.hexlify(item).decode('utf-8') for item in byte_list]

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/sign', methods=['GET', 'POST'])
def sign():
    if(request.method=='POST'):
        message = request.form['message']
        keypair = keygen()
        sig = signfunc(message, keypair[SK])

        # Chuyển đổi danh sách các bytes thành danh sách các chuỗi hex
        sig_hex = [binascii.hexlify(item).decode('utf-8') for item in sig]

        with open('./saveFile/sig.json', 'w') as sig_file:
            json.dump(sig_hex, sig_file)
        pk_hex = [bytes_to_hex_string(item) for item in keypair[PK]]
        with open('./saveFile/keypair.json', 'w') as pk_file:
            json.dump(pk_hex, pk_file)
        return render_template('resulSign.html', sig_txt=sig, key_txt=keypair[PK])
    return render_template('sign.html')


def read_signature_from_json(file_path):
    with open(file_path, 'r') as sig_file:
        sig_hex = json.load(sig_file)
    # Chuyển đổi danh sách các chuỗi hex thành danh sách các bytes
    sig = [binascii.unhexlify(item) for item in sig_hex]
    return sig
def read_keypair_from_json(file_path):
    with open(file_path, 'r') as pk_file:
        pk_hex = json.load(pk_file)
    
    # Chuyển đổi danh sách các chuỗi hex thành danh sách danh sách các bytes
    pk = [[binascii.unhexlify(item) for item in row] for row in pk_hex]
    return pk

@app.route('/verify', methods=['GET', 'POST'])
def verify():
    return render_template('verify.html')

@app.route('/upload', methods=['POST'])
def upload():
    # Kiểm tra xem cả hai file JSON đã được gửi lên chưa
    if 'json_file_sig' not in request.files or 'json_file_key' not in request.files:
        return 'Vui lòng chọn cả hai file JSON để tải lên.'
    message = request.form['message']
    filesig = request.files['json_file_sig']
    filekey = request.files['json_file_key']

    # Kiểm tra xem cả hai file có hợp lệ không và lưu chúng vào thư mục
    if (
        filesig.filename != '' and filesig.filename.endswith('.json') and
        filekey.filename != '' and filekey.filename.endswith('.json')
    ):
        filename_sig = os.path.join(app.config['UPLOAD_FOLDER'], filesig.filename)
        filesig.save(filename_sig)
        filename_key = os.path.join(app.config['UPLOAD_FOLDER'], filekey.filename)
        filekey.save(filename_key)
        result = verifyfunc(message,read_signature_from_json(filename_sig),read_keypair_from_json(filename_key))
        if(result):
            return render_template('trueVerify.html')
        else:
            return render_template('falseVerify.html')
    return 'Cả hai file không hợp lệ. Vui lòng chọn file JSON để tải lên.'
if __name__ == '__main__':
    app.run(debug=True)